<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

    // ====================================================== //
    //         START----> INI UNTUK ADMIN                     //
    //========================================================//

    // BAGIAN ADMIN

    public function getDataAdmin()
    {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->join('level', 'admin.id_level = level.id_level');
        return $this->db->get()->result_array();
    }

    public function getDataAdmin_id($id)
    {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->join('level', 'admin.id_level = level.id_level');
        $this->db->where(['id_admin' => $id]);
        return $this->db->get()->row_array();
    }

    public function editProfileAdmin()
    {
        $id_admin = $this->input->post('id');
        $nama = $this->input->post('nama');
        $username = $this->input->post('username');
        $jk = $this->input->post('jk');
        $ttl = $this->input->post('ttl');
        $telp = $this->input->post('telp');
        $alamat = $this->input->post('alamat');
        $email = $this->input->post('email');

        //cek jika ada gambar
        $upload_image = $_FILES['foto'];

        if ($upload_image) {
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']      = '2048';
            $config['overwrite'] = true;
            $config['file_name']       = $this->input->post('nama');
            $config['upload_path']   = './uploads/admin/';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {

                // upload foto dan edit di database

                if (!empty($_FILES["foto"]["name"])) {
                    $new_image = $this->upload->data('file_name');
                } else {
                    $new_image = $this->input->post("old_image");
                }
                $this->db->set('foto_admin', $new_image);
            } else {
                $new_image = $this->input->post('old_image');
                echo $this->upload->display_errors();
            }
        }

        //update nama, email dan telepon
        $this->db->set('nama_admin', $nama);
        $this->db->set('username_admin', $username);
        $this->db->set('jk', $jk);
        $this->db->set('ttl', $ttl);
        $this->db->set('telp', $telp);
        $this->db->set('alamat', $alamat);
        $this->db->set('email', $email);
        $this->db->where('id_admin', $id_admin);
        $this->db->update('admin');

        $data = [
            'foto' => $new_image,
            'nama' => $nama
        ];
        $this->session->unset_userdata(['foto','nama']);
        $this->session->set_userdata($data);
    }


    // BAGIAN DOSEN

    public function getDataDosen()
    {
        $this->db->select('*');
        $this->db->from('dosen');
        $this->db->join('level', 'dosen.id_level = level.id_level');
        return $this->db->get()->result_array();
    }

    public function getDataDosen_id($id)
    {
        $this->db->select('*');
        $this->db->from('dosen');
        $this->db->join('level', 'dosen.id_level = level.id_level');
        $this->db->where(['nid' => $id]);
        return $this->db->get()->row_array();
    }

    public function tambahDataDosen()
    {
        $data = [

            "nid" => $this->input->post('nid', true),
            "nama_dosen" => $this->input->post('nama_dosen', true),
            "ttl" => $this->input->post('ttl', true),
            "jk" => $this->input->post('jk', true),
            "email" => $this->input->post('email', true),
            "telp" => $this->input->post('telp', true),
            "alamat" => $this->input->post('alamat', true),
            "foto" => 'default.png',
            "id_level" => '2'

        ];
        $this->db->insert('dosen', $data);
    }

    public function editDataDosen()
    {

        $upload_image = $_FILES['foto'];

        if ($upload_image) {
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']      = '2048';
            $config['overwrite'] = true;
            $config['file_name']       = $this->input->post('nama');
            $config['upload_path']   = './assets/dist/img/profile/';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {

                // upload foto dan edit di database

                if (!empty($_FILES["foto"]["name"])) {
                    $new_image = $this->upload->data('file_name');
                } else {
                    $new_image = $this->input->post("old_image");
                }
                $this->db->set('foto', $new_image);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = [

            "nama_dosen" => $this->input->post('nama_dosen', true),
            "ttl" => $this->input->post('ttl', true),
            "jk" => $this->input->post('jk', true),
            "email" => $this->input->post('email', true),
            "telp" => $this->input->post('telp', true),
            "alamat" => $this->input->post('alamat', true),
            "foto" => 'dosen.png',
            "id_level" => '2'
        ];

        $this->db->where('nid', $this->input->post('id'));
        $this->db->update('dosen', $data);
    }

    public function hapusDataDosen($id)
    {
        $this->db->where('nid', $id);
        $this->db->delete('dosen');
    }


    //BAGIAN KARYAWAN ATAU STAFF

    public function getDataStaff()
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->join('level', 'staff.id_level = level.id_level');
        return $this->db->get()->result_array();
    }

    public function getDataStaff_id($id)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->join('level', 'staff.id_level = level.id_level');
        $this->db->where(['nip' => $id]);
        return $this->db->get()->row_array();
    }

    public function tambahDataStaff()
    {
        $upload_image = $_FILES['foto'];

        if ($upload_image) {
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']      = '2048';
            $config['overwrite'] = true;
            $config['file_name']       = $this->input->post('nama');
            $config['upload_path']   = './assets/dist/img/profile/';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {

                // upload foto dan edit di database

                if (!empty($_FILES["foto"]["name"])) {
                    $new_image = $this->upload->data('file_name');
                } else {
                    $new_image = $this->input->post("old_image");
                }
                $this->db->set('foto', $new_image);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = [

            "nip" => $this->input->post('nip', true),
            "nama_staff" => $this->input->post('nama_staff', true),
            "bagian" => $this->input->post('bagian', true),
            "ttl" => $this->input->post('ttl', true),
            "jk" => $this->input->post('jk', true),
            "email" => $this->input->post('email', true),
            "telp" => $this->input->post('telp', true),
            "alamat" => $this->input->post('alamat', true),
            "foto" => $new_image,
            "id_level" => '3'

        ];
        $this->db->insert('staff', $data);
    }

    public function editDataStaff($table,$data,$field)
    {
        $this->db->where($field);
        $this->db->update($table, $data);
    }

    public function hapusDataStaff($id)
    {
        $this->db->where('nip', $id);
        $this->db->delete('staff');
    }


    //BAGIAN MAHASISWA

    public function getDataMahasiswa()
    {
        $this->db->select('*');
        $this->db->from('mahasiswa');
        $this->db->join('level', 'mahasiswa.id_level = level.id_level');
        return $this->db->get()->result_array();
    }

    public function getDataMahasiswa_id($id)
    {
        $this->db->select('*');
        $this->db->from('mahasiswa');
        $this->db->join('level', 'mahasiswa.id_level = level.id_level');
        $this->db->where(['nim' => $id]);
        return $this->db->get()->row_array();
    }

    public function tambahDataMahasiswa()
    {
        $data = [

            "nim" => $this->input->post('nim', true),
            "nama_mahasiswa" => $this->input->post('nama_mahasiswa', true),
            "prodi" => $this->input->post('prodi', true),
            "angkatan" => $this->input->post('angkatan', true),
            "ttl" => $this->input->post('ttl', true),
            "jk" => $this->input->post('jk', true),
            "email" => $this->input->post('email', true),
            "telp" => $this->input->post('telp', true),
            "alamat" => $this->input->post('alamat', true),
            "foto" => 'default.png',
            "id_level" => '4'

        ];
        $this->db->insert('mahasiswa', $data);
    }

    public function editDataMahasiswa()
    {
        $data = [

            "nama_mahasiswa" => $this->input->post('nama_mahasiswa', true),
            "prodi" => $this->input->post('prodi', true),
            "angkatan" => $this->input->post('angkatan', true),
            "ttl" => $this->input->post('ttl', true),
            "jk" => $this->input->post('jk', true),
            "email" => $this->input->post('email', true),
            "telp" => $this->input->post('telp', true),
            "alamat" => $this->input->post('alamat', true),
            "foto" => 'mahasiswa.png',
            "id_level" => '4'
        ];

        $this->db->where('nim', $this->input->post('id'));
        $this->db->update('mahasiswa', $data);
    }

    public function hapusDataMahasiswa($id)
    {
        $this->db->where('nim', $id);
        $this->db->delete('mahasiswa');
    }



    //DATA USER//

    public function tambahUserDosen()
    {
        $data = [
            "username" => $this->input->post('nid', true),
            "password" => sha1($this->input->post('nid', true)),
            "id_level" => '2',
        ];
        $this->db->insert('user', $data);
    }

    public function hapusUserDosen($id)
    {
        $this->db->where('username', $id);
        $this->db->delete('user');
    }


    public function tambahUserStaff()
    {
        $data = [
            "username" => $this->input->post('nip', true),
            "password" => sha1($this->input->post('nip', true)),
            "id_level" => '3',
        ];
        $this->db->insert('user', $data);
    }

    public function hapusUserStaff($id)
    {
        $this->db->where('username', $id);
        $this->db->delete('user');
    }

    public function tambahUserMahasiswa()
    {
        $data = [
            "username" => $this->input->post('nim', true),
            "password" => sha1($this->input->post('nim', true)),
            "id_level" => '4',
        ];
        $this->db->insert('user', $data);
    }

    public function hapusUserMahasiswa($id)
    {
        $this->db->where('username', $id);
        $this->db->delete('user');
    }

    // ====================================================== //
    //         FINISH----> INI UNTUK ADMIN                     //
    //========================================================//

    // ====================================================== //
    //         START----> INI UNTUK USER                     //
    //========================================================//

    //Dosen

    public function editProfileDosen()
    {
        $nama = $this->input->post('nama_dosen');
        $ttl = $this->input->post('ttl');
        $jk = $this->input->post('jk');
        $email = $this->input->post('email');
        $telp = $this->input->post('telp');
        $alamat = $this->input->post('alamat');

        //cek jika ada gambar
        $upload_image = $_FILES['foto'];

        if ($upload_image) {
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']      = '2048';
            $config['file_name']       = $this->input->post('nama_dosen');
            $config['upload_path']   = './assets/dist/img/profile/';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {

                // upload foto dan edit di database
                $new_image = $this->upload->data('file_name');
                $this->db->set('foto', $new_image);
            } else {
                echo $this->upload->display_errors();
            }
        }

        //update nama, email dan telepon
        $this->db->set('nama_dosen', $nama);
        $this->db->set('ttl', $ttl);
        $this->db->set('jk', $jk);
        $this->db->set('email', $email);
        $this->db->set('telp', $telp);
        $this->db->set('alamat', $alamat);
        $this->db->where('nid', $this->input->post('id'));
        $this->db->update('dosen');

        $data = [
            'foto' => $new_image,
            'nama' => $nama
        ];
        $this->session->set_userdata($data);
    }

    //Staff

    public function editProfileStaff()
    {
        $nama = $this->input->post('nama_staff');
        $ttl = $this->input->post('ttl');
        $jk = $this->input->post('jk');
        $email = $this->input->post('email');
        $telp = $this->input->post('telp');
        $alamat = $this->input->post('alamat');

        //cek jika ada gambar
        $upload_image = $_FILES['foto'];

        if ($upload_image) {
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']      = '2048';
            $config['file_name']       = $this->input->post('nama_staff');
            $config['upload_path']   = './assets/dist/img/profile/';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {

                // upload foto dan edit di database
                $new_image = $this->upload->data('file_name');
                $this->db->set('foto', $new_image);
            } else {
                echo $this->upload->display_errors();
            }
        }

        //update nama, email dan telepon
        $this->db->set('nama_staff', $nama);
        $this->db->set('ttl', $ttl);
        $this->db->set('jk', $jk);
        $this->db->set('email', $email);
        $this->db->set('telp', $telp);
        $this->db->set('alamat', $alamat);
        $this->db->where('nip', $this->input->post('id'));
        $this->db->update('staff');

        $data = [
            'foto' => $new_image,
            'nama' => $nama
        ];
        $this->session->set_userdata($data);
    }

    //Mahasiswa

    public function editProfileMahasiswa()
    {
        $nama = $this->input->post('nama_mahasiswa');
        $ttl = $this->input->post('ttl');
        $jk = $this->input->post('jk');
        $email = $this->input->post('email');
        $telp = $this->input->post('telp');
        $alamat = $this->input->post('alamat');

        //cek jika ada gambar
        $upload_image = $_FILES['foto'];

        if ($upload_image) {
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']      = '2048';
            $config['file_name']       = $this->input->post('nama_mahasiswa');
            $config['upload_path']   = './assets/dist/img/profile/';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {

                // upload foto dan edit di database
                $new_image = $this->upload->data('file_name');
                $this->db->set('foto', $new_image);
            } else {
                echo $this->upload->display_errors();
            }
        }

        //update nama, email dan telepon
        $this->db->set('nama_mahasiswa', $nama);
        $this->db->set('ttl', $ttl);
        $this->db->set('jk', $jk);
        $this->db->set('email', $email);
        $this->db->set('telp', $telp);
        $this->db->set('alamat', $alamat);
        $this->db->where('nim', $this->input->post('id'));
        $this->db->update('mahasiswa');

        $data = [
            'foto' => $new_image,
            'nama' => $nama
        ];
        $this->session->set_userdata($data);
    }


    public function update($table, $field, $data){
        $this->db->where($field);
        $this->db->update($table,$data);
    }
    public function checkpass($table, $field, $field2){
        $this->db->where($field);
        $this->db->where($field2);
        return $this->db->get($table);
    }
}
