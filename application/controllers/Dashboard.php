<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends My_Controller
{
    public function index()
    {
        $data['title'] = "E-Quistionnaire";
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $this->pagging('dashboard/admin', $data);
    }

    public function dosen()
    {
        $data['title'] = "E-Quistionnaire";
        $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $this->pagging('dashboard/dosen', $data);
    }

    public function staff()
    {
        $data['title'] = "E-Quistionnaire";
        $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $this->pagging('dashboard/staff', $data);
    }

    public function mahasiswa()
    {
        $data['title'] = "E-Quistionnaire";
        $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $this->pagging('dashboard/mahasiswa', $data);
    }
}
