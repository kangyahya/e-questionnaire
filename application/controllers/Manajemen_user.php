<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manajemen_user extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    // ====================================================== //
    //            START ---->  INI UNTUK ADMIN                //
    //========================================================//


    // BAGIAN DOSEN
    public function index()
    {
        redirect('manajemen_user/dosen');
    }
    public function dosen(){
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'Manajemen User | Dosen';
        $data['dosen'] = $this->user_model->getDataDosen();
        $this->pagging('manajemen_user/dosen/data_dosen', $data);
    }
    public function tambah_dosen()
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'E-Quistionnaire | Tambah Dosen';

        //form validasi set rules

        $this->form_validation->set_rules('nid', 'Nid', 'required|is_unique[dosen.nid]|numeric', [
            'required' => 'NID tidak boleh kosong',
            'is_unique' => 'NID sudah dipakai',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        $this->form_validation->set_rules('nama_dosen', 'Nama_dosen', 'required|trim', [
            'required' => 'Nama tidak boleh kosong'
        ]);

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[dosen.email]', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Harus di isi dengan format email',
            'is_unique' => 'Email sudah dipakai'
        ]);

        $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
            'required' => 'No Telepon tidak boleh kosong',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        $this->form_validation->set_rules('jk', 'Jk', 'required', [
            'required' => 'Jenis Kelamin tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
            'required' => 'Tanggal Lahir tidak boleh kosong',
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == false) {
            $this->pagging('manajemen_user/dosen/tambah_dosen', $data);

            //jika form validasi benar
        } else {
            $this->user_model->tambahDataDosen();
            $this->user_model->tambahUserDosen();
            $this->session->set_flashdata('message', ' Di Tambahkan');
            redirect('manajemen_user/dosen');
        }
    }

    public function edit_dosen($id)
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'E-Quistionnaire | Dosen';
        $data['dosen'] = $this->user_model->getDataDosen_id($id);

        //form validasi set rules
        $this->form_validation->set_rules('nama_dosen', 'Nama_dosen', 'required|trim', [
            'required' => 'Nama tidak boleh kosong'
        ]);

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Harus di isi dengan format email',
        ]);

        $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
            'required' => 'No Telepon tidak boleh kosong',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        $this->form_validation->set_rules('jk', 'Jk', 'required', [
            'required' => 'Jenis Kelamin tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
            'required' => 'Tanggal Lahir tidak boleh kosong',
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('manajemen_user/dosen/edit_dosen', $data);

            //jika form validasi benar
        } else {
            $this->user_model->editDataDosen();
            $this->session->set_flashdata('message', ' Di Edit');
            redirect('manajemen_user/dosen');
        }
    }

    public function hapus_dosen($id)
    {
        $this->user_model->hapusDataDosen($id);
        $this->user_model->hapusUserDosen($id);
        $this->session->set_flashdata('message', ' Di Hapus');
        redirect('manajemen_user/dosen');
    }

    public function detail_dosen($id)
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'E-Quistionnaire | Detail Dosen';
        $data['dosen'] = $this->user_model->getDataDosen_id($id);
        $this->pagging('manajemen_user/dosen/detail_dosen', $data);
    }



    // BAGIAN KARYAWAN ATAU STAFF
    public function staff()
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'Manajemen User | Karyawan/ Staff';
        $data['staff'] = $this->user_model->getDataStaff();
        $this->pagging('manajemen_user/staff/data_staff', $data);
    }

    public function tambah_staff()
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'E-Quistionnaire | Tambah Karyawan/ Staff';

        //form validasi set rules

        $this->form_validation->set_rules('nip', 'Nip', 'required|is_unique[staff.nip]|numeric', [
            'required' => 'NIP tidak boleh kosong',
            'is_unique' => 'NIP sudah dipakai',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        $this->form_validation->set_rules('nama_staff', 'Nama_staff', 'required|trim', [
            'required' => 'Nama tidak boleh kosong'
        ]);

        $this->form_validation->set_rules('bagian', 'Bagian', 'required', [
            'required' => 'Bagian tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[staff.email]', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Harus di isi dengan format email',
            'is_unique' => 'Email sudah dipakai'
        ]);

        $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
            'required' => 'No Telepon tidak boleh kosong',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        $this->form_validation->set_rules('jk', 'Jk', 'required', [
            'required' => 'Jenis Kelamin tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
            'required' => 'Tanggal Lahir tidak boleh kosong',
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == false) {
            $this->pagging('manajemen_user/staff/tambah_staff', $data);

            //jika form validasi benar
        } else {
            $this->user_model->tambahDataStaff();
            $this->user_model->tambahUserStaff();
            $this->session->set_flashdata('message', ' Di Tambahkan');
            redirect('manajemen_user/staff');
        }
    }

    public function edit_staff($id)
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'E-Quistionnaire | Edit Karyawan/ Staff';
        $data['staff'] = $this->user_model->getDataStaff_id($id);

        //form validasi set rules

        $this->form_validation->set_rules('nama_staff', 'Nama_staff', 'required|trim', [
            'required' => 'Nama tidak boleh kosong'
        ]);

        $this->form_validation->set_rules('bagian', 'Bagian', 'required', [
            'required' => 'Bagian tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Harus di isi dengan format email',
        ]);

        $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
            'required' => 'No Telepon tidak boleh kosong',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        $this->form_validation->set_rules('jk', 'Jk', 'required', [
            'required' => 'Jenis Kelamin tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
            'required' => 'Tanggal Lahir tidak boleh kosong',
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('manajemen_user/staff/edit_staff', $data);

            //jika form validasi benar
        } else {
            $this->user_model->editDataStaff();
            $this->session->set_flashdata('message', ' Di Edit');
            redirect('manajemen_user/staff');
        }
    }


    public function hapus_staff($id)
    {
        $this->user_model->hapusDataStaff($id);
        $this->user_model->hapusUserStaff($id);
        $this->session->set_flashdata('message', ' Di Hapus');
        redirect('manajemen_user/staff');
    }

    public function detail_staff($id)
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'E-Quistionnaire | Detail Karyawan/ Staff';
        $data['staff'] = $this->user_model->getDataStaff_id($id);
        $this->pagging('manajemen_user/staff/detail_staff', $data);
    }


    // BAGIAN MAHASISWA
    public function mahasiswa()
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'Manajemen User | Mahasiswa';
        $data['mahasiswa'] = $this->user_model->getDataMahasiswa();
        $this->pagging('manajemen_user/mahasiswa/data_mahasiswa', $data);
    }

    public function tambah_mahasiswa()
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'E-Quistionnaire | Tambah Mahasiswa';

        //form validasi set rules

        $this->form_validation->set_rules('nim', 'Nim', 'required|is_unique[mahasiswa.nim]|numeric', [
            'required' => 'NIM tidak boleh kosong',
            'is_unique' => 'NIM sudah dipakai',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        $this->form_validation->set_rules('nama_mahasiswa', 'Nama_mahasiswa', 'required|trim', [
            'required' => 'Nama tidak boleh kosong'
        ]);

        $this->form_validation->set_rules('prodi', 'Prodi', 'required', [
            'required' => 'Prodi tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('angkatan', 'Angkatan', 'required', [
            'required' => 'Tahun Akademik tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[mahasiswa.email]', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Harus di isi dengan format email',
            'is_unique' => 'Email sudah dipakai'
        ]);

        $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
            'required' => 'No Telepon tidak boleh kosong',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        $this->form_validation->set_rules('jk', 'Jk', 'required', [
            'required' => 'Jenis Kelamin tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
            'required' => 'Tanggal Lahir tidak boleh kosong',
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == false) {
            $this->pagging('manajemen_user/mahasiswa/tambah_mahasiswa', $data);

            //jika form validasi benar
        } else {
            $this->user_model->tambahDataMahasiswa();
            $this->user_model->tambahUserMahasiswa();
            $this->session->set_flashdata('message', ' Di Tambahkan');
            redirect('manajemen_user/mahasiswa');
        }
    }

    public function edit_mahasiswa($id)
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'E-Quistionnaire | Edit Mahasiswa';
        $data['mahasiswa'] = $this->user_model->getDataMahasiswa_id($id);

        //form validasi set rules

        $this->form_validation->set_rules('nama_mahasiswa', 'Nama_mahasiswa', 'required|trim', [
            'required' => 'Nama tidak boleh kosong'
        ]);

        $this->form_validation->set_rules('prodi', 'Prodi', 'required', [
            'required' => 'Prodi tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('angkatan', 'Angkatan', 'required', [
            'required' => 'Tahun Akademik tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Harus di isi dengan format email',
        ]);

        $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
            'required' => 'No Telepon tidak boleh kosong',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        $this->form_validation->set_rules('jk', 'Jk', 'required', [
            'required' => 'Jenis Kelamin tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
            'required' => 'Tanggal Lahir tidak boleh kosong',
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('manajemen_user/mahasiswa/edit_mahasiswa', $data);

            //jika form validasi benar
        } else {
            $this->user_model->editDataMahasiswa();
            $this->session->set_flashdata('message', ' Di Edit');
            redirect('manajemen_user/mahasiswa');
        }
    }


    public function hapus_mahasiswa($id)
    {
        $this->user_model->hapusDataMahasiswa($id);
        $this->user_model->hapusUserMahasiswa($id);
        $this->session->set_flashdata('message', ' Di Hapus');
        redirect('manajemen_user/mahasiswa');
    }

    public function detail_mahasiswa($id)
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'E-Quistionnaire | Detail Mahasiswa';
        $data['mahasiswa'] = $this->user_model->getDataMahasiswa_id($id);
        $this->pagging('manajemen_user/mahasiswa/detail_mahasiswa', $data);
    }

    // ======================================================  //
    //            FINISH ---->  INI UNTUK ADMIN                //
    //========================================================//

}
