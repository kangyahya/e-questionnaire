<?php
class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    function index()
    {
        $data['title'] = 'E-Questionnaire | Login User';

        //SESSION
        if ($this->session->userdata('username')) {
            redirect('dashboard');
        }

        //form validasi set rules
        $this->form_validation->set_rules('username', 'username', 'required|trim', [
            'required' => 'Username tidak boleh kosong'
        ]);
        $this->form_validation->set_rules('password', 'password', 'required', [
            'required' => 'Password tidak boleh kosong'
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == false) {
            $this->load->view('login/login', $data);

            //jika form validasi benar
        } else {
            $username = $this->input->post('username');
            $password = sha1($this->input->post('password'));
            $user = $this->db->get_where('user', ['username' => $username])->row_array();
            $level = $this->db->get_where('level', ['id_level' => $user['id_level']])->row_array();
            //jika user Dosen ada
            if ($user) {
                //cek password
                if ($password == $user['password']) {
                    if ($user['id_level'] == 2) {
                        $userdosen = $this->db->get_where('dosen', ['nid' => $username])->row_array();
                        $data = [
                            'username' => $user['username'],
                            'id_level' => $user['id_level'],
                            'nama' => $userdosen['nama_dosen'],
                            'foto' => $userdosen['foto'],
                            'level' => $level['level']
                        ];
                        $this->session->set_userdata($data);
                        redirect('dashboard/dosen');
                    } else if ($user['id_level'] == 3) {
                        $userstaff = $this->db->get_where('staff', ['nip' => $username])->row_array();
                        $data = [
                            'username' => $user['username'],
                            'id_level' => $user['id_level'],
                            'nama' => $userstaff['nama_staff'],
                            'foto' => $userstaff['foto'],
                            'level' => $level['level']
                        ];
                        $this->session->set_userdata($data);
                        redirect('dashboard/staff');
                    } else {
                        $usermhs = $this->db->get_where('mahasiswa', ['nim' => $username])->row_array();
                        $data = [
                            'username' => $user['username'],
                            'id_level' => $user['id_level'],
                            'nama' => $usermhs['nama_mahasiswa'],
                            'foto' => $usermhs['foto'],
                            'level' => $level['level']
                        ];
                        $this->session->set_userdata($data);
                        redirect('dashboard/mahasiswa');
                    }
                    //password salah
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" align="center" role="alert">Password salah!</div>');
                    redirect('login');
                }
                //User tidak ada
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" align="center" role="alert">Akun ini tidak ada di database</div>');
                redirect('login');
            }
        }
    }
    function admin()
    {
        $data['title'] = 'E-Questionnaire | Login User';
        //SESSION
        if ($this->session->userdata('username')) {
            redirect('dashboard');
        }
        //form validasi set rules
        $this->form_validation->set_rules('username', 'username', 'required|trim', [
            'required' => 'Username tidak boleh kosong'
        ]);
        $this->form_validation->set_rules('password', 'password', 'required', [
            'required' => 'Password tidak boleh kosong'
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == false) {
            $this->load->view('login/login', $data);

            //jika form validasi benar
        } else {
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $admin = $this->db->get_where('admin', ['username_admin' => $username])->row_array();
            //cek level
            $level = $this->db->get_where('level', ['id_level' => $admin['id_level']])->row_array();
            //jika user ada
            if ($admin) {
                //cek password
                if ($password == $admin['password_admin']) {
                    $data = [
                        'id' => $admin['id_admin'],
                        'username' => $admin['username_admin'],
                        'nama' => $admin['nama_admin'],
                        'jk' => $admin['jk'],
                        'foto' => $admin['foto_admin'],
                        'id_level' => $admin['id_level'],
                        'level' => $level['level'],
                    ];
                    $this->session->set_userdata($data);
                    redirect('dashboard');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" align="center" role="alert">Password salah!</div>');
                    redirect('login');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" align="center" role="alert">Akun ini tidak ada di database</div>');
                redirect('login');
            }
        }
    }
    public function logout()
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('message', '<div class="alert alert-success" align="center" role="alert">
        Logout Berhasil!</div>');
        redirect('login');
    }

    // function changepw_user()
    // {
    //     $data['title'] = 'E-Quistionnaire | Login User';
    //     if ($this->session->userdata('username')) {
    //         redirect('dashboard');
    //     }
    //     //form validasi set rules
    //     $this->form_validation->set_rules('username', 'username', 'required|trim', [
    //         'required' => 'Username lama tidak boleh kosong'
    //     ]);
    //     $this->form_validation->set_rules('pw_baru', 'pw_baru', 'required|min_length[3]', [
    //         'required' => 'Password baru tidak boleh kosong',
    //         'min_length' => 'Harus di isi minimal 3 Karakter',
    //     ]);
    //     $this->form_validation->set_rules('pw_baru2', 'pw_baru2', 'required|min_length[3]|matches[pw_baru]', [
    //         'required' => 'Ulangi password tidak boleh kosong',
    //         'min_length' => 'Harus di isi minimal 3 Karakter',
    //         'matches' => 'Password tidak sama'
    //     ]);

    //     //jika form validasi salah
    //     if ($this->form_validation->run() == FALSE) {
    //         $this->pagging('login/login', $data);

    //         //jika form validasi benar
    //     } else {
    //         $username = $this->input->post('username');
    //         $password_baru1 = $this->input->post('pw_baru1');
    //         $password_baru2 = $this->input->post('pw_baru2');
    //         if (!$username($data['user']['username'])) {
    //             $this->session->set_flashdata('message', '<div class="alert alert-danger" align="center" role="alert">Username Tidak Ada </div>');
    //             redirect('login');
    //         } else {
    //             //password OK
    //             $password_sekarang = sha1($password_baru, PASSWORD_DEFAULT);

    //             $this->db->set('password', $password_sekarang);
    //             $this->db->where('username', $this->session->userdata('id'));
    //             $this->db->update('user');

    //             redirect('login');
    //         }
    //     }
    // }
}
