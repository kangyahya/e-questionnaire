<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manajemen User
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Mahasiswa</h3>
                    </div>

                    <!-- Form -->
                    <br />
                    <form action="<?php echo site_url('manajemen_user/tambah_mahasiswa'); ?>" method="post" class="form-horizontal">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="nim" class="col-sm-3 control-label">NIM</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="nim" name="nim" value="<?php echo set_value('nim'); ?>" placeholder="Masukkan Nomor Induk Mahasiswa (NIM) *">
                                    <?php echo form_error('nim', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="nama_mahasiswa" class="col-sm-3 control-label">Nama </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="nama_mahasiswa" name="nama_mahasiswa" value="<?php echo set_value('nama_mahasiswa'); ?>" placeholder="Masukkan Nama Mahasiswa *">
                                    <?php echo form_error('nama_mahasiswa', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Prodi</label>
                                <div class="col-md-6 col-sm-9 col-xs-12">
                                    <select class="form-control" name="prodi">
                                        <option value="">Pilih Pogram Studi !!</option>
                                        <option value="Teknik Informatika">Teknik Informatika</option>
                                        <option value="Sistem Informasi Komputerisasi Akuntansi">Sistem Informasi Komputerisasi Akuntansi</option>
                                        <option value="Desain Komunikasi Visual">Desain Komunikasi Visual</option>
                                        <option value="Komputerisasi Akuntasi">Komputerisasi Akuntasi</option>
                                        <option value="Manajemen Informatika">Manajemen Informatika</option>
                                        <option value="Manajemen">Manajemen</option>
                                        <option value="Akuntansi">Akuntansi</option>
                                        <option value="Manajemen Bisnis">Manajemen Bisnis</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="angkatan" class="col-sm-3 control-label">Tahun Akademik</label>
                                <div class="col-sm-6">
                                    <input type="text" maxlength="4" class="form-control" id="angkatan" name="angkatan" value="<?php echo set_value('angkatan'); ?>" placeholder="Masukkan Angkatan Mahasiswa *">
                                    <?php echo form_error('angkatan', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="ttl" class="col-sm-3 control-label">Tanggal Lahir</label>
                                <div class="col-sm-6">
                                    <input type="date" class="form-control" id="ttl" name="ttl" value="<?= date('Y-m-d') ?>" placeholder="Masukkan Tanggal Lahir Mahasiswa *">
                                    <?php echo form_error('ttl', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin</label>
                                <div class="col-md-6 col-sm-9 col-xs-12">
                                    <select class="form-control" name="jk">
                                        <option value="">Choose Your Option !!</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="Masukkan Email Mahasiswa *">
                                    <?php echo form_error('email', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="telp" class="col-sm-3 control-label">No Telepon</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="telp" name="telp" value="<?php echo set_value('telp'); ?>" placeholder="Masukkan Nomor Telepon Mahasiswa *">
                                    <?php echo form_error('telp', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo set_value('alamat'); ?>" placeholder="Masukkan Alamat Mahasiswa *">
                                    <?php echo form_error('alamat', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?php echo site_url('manajemen_user/mahasiswa'); ?>" button type="submit" class="btn btn-warning"><i class="fa fa-rotate-left"></i> Kembali</a>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>