<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manajemen Mahasiswa
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Mahasiswa</h3>
                    </div>

                    <!-- Form -->
                    <br />
                    <form method="post" action="" class="form-horizontal">
                        <input type="hidden" name="id" value="<?php echo $mahasiswa['nim']; ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="nama" class="col-sm-3 control-label">NIM</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="nim" name="nim" value="<?php echo $mahasiswa['nim']; ?>" disabled>
                                    <?php echo form_error('nim', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="nama_mahasiswa" class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="nama_mahasiswa" name="nama_mahasiswa" value="<?php echo $mahasiswa['nama_mahasiswa']; ?>" placeholder="Masukkan Nama Mahasiswa *">
                                    <?php echo form_error('nama_mahasiswa', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Prodi</label>
                                <div class="col-md-6 col-sm-9 col-xs-12">
                                    <select class="form-control" name="prodi">
                                        <?php if ($mahasiswa['prodi'] == 'Teknik Informatika') {
                                            $teknikinformatika = "selected";
                                            $sisteminformasikomputerakuntansi = "";
                                            $desainkomunikasivisual = "";
                                            $komputerisasiakuntansi = "";
                                            $manajemeninformatika = "";
                                            $manajemen = "";
                                            $akuntansi = "";
                                            $manajemenbisnis = "";
                                        } elseif ($mahasiswa['prodi'] == 'Sistem Informasi Akuntansi') {
                                            $teknikinformatika = "";
                                            $sisteminformasikomputerakuntansi = "selected";
                                            $desainkomunikasivisual = "";
                                            $komputerisasiakuntansi = "";
                                            $manajemeninformatika = "";
                                            $manajemen = "";
                                            $akuntansi = "";
                                            $manajemenbisnis = "";
                                        } elseif ($mahasiswa['prodi'] == 'Desain Komunikasi Visual') {
                                            $teknikinformatika = "";
                                            $sisteminformasikomputerakuntansi = "";
                                            $desainkomunikasivisual = "selected";
                                            $komputerisasiakuntansi = "";
                                            $manajemeninformatika = "";
                                            $manajemen = "";
                                            $akuntansi = "";
                                            $manajemenbisnis = "";
                                        } elseif ($mahasiswa['prodi'] == 'Komputerisasi Akuntansi') {
                                            $teknikinformatika = "";
                                            $sisteminformasikomputerakuntansi = "";
                                            $desainkomunikasivisual = "";
                                            $komputerisasiakuntansi = "selected";
                                            $manajemeninformatika = "";
                                            $manajemen = "";
                                            $akuntansi = "";
                                            $manajemenbisnis = "";
                                        } elseif ($mahasiswa['prodi'] == 'Manajemen Informatika') {
                                            $teknikinformatika = "";
                                            $sisteminformasikomputerakuntansi = "";
                                            $desainkomunikasivisual = "";
                                            $komputerisasiakuntansi = "";
                                            $manajemeninformatika = "selected";
                                            $manajemen = "";
                                            $akuntansi = "";
                                            $manajemenbisnis = "";
                                        } elseif ($mahasiswa['prodi'] == 'Manajemen') {
                                            $teknikinformatika = "";
                                            $sisteminformasikomputerakuntansi = "";
                                            $desainkomunikasivisual = "";
                                            $komputerisasiakuntansi = "";
                                            $manajemeninformatika = "";
                                            $manajemen = "selected";
                                            $akuntansi = "";
                                            $manajemenbisnis = "";
                                        } elseif ($mahasiswa['prodi'] == 'Akuntansi') {
                                            $teknikinformatika = "";
                                            $sisteminformasikomputerakuntansi = "";
                                            $desainkomunikasivisual = "";
                                            $komputerisasiakuntansi = "";
                                            $manajemeninformatika = "";
                                            $manajemen = "";
                                            $akuntansi = "selected";
                                            $manajemenbisnis = "";
                                        } else {
                                            $teknikinformatika = "";
                                            $sisteminformasikomputerakuntansi = "";
                                            $desainkomunikasivisual = "";
                                            $komputerisasiakuntansi = "";
                                            $manajemeninformatika = "";
                                            $manajemen = "";
                                            $akuntansi = "";
                                            $manajemenbisnis = "selected";
                                        }; ?>
                                        <option <?php echo $teknikinformatika; ?> value="Teknik Informatika"> Teknik Informatika</option>
                                        <option <?php echo $sisteminformasikomputerakuntansi; ?> value="Sistem Informasi Komputerisasi Akuntansi"> Sistem Informasi Komputerisasi Akuntansi</option>
                                        <option <?php echo $desainkomunikasivisual; ?> value="Desain Komunikasi Visual"> Desain Komunikasi Visual</option>
                                        <option <?php echo $komputerisasiakuntansi; ?> value="Komputerisasi Akuntansi"> Komputerisasi Akuntansi</option>
                                        <option <?php echo $manajemeninformatika; ?> value="Manajemen Informatika"> Manajemen Informatika</option>
                                        <option <?php echo $manajemen; ?> value="Manajemen"> Manajemen</option>
                                        <option <?php echo $akuntansi; ?> value="Akuntansi"> Akuntansi</option>
                                        <option <?php echo $manajemenbisnis; ?> value="Manajemen Bisnis"> Manajemen Bisnis</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="angkatan" class="col-sm-3 control-label">Tahun Akademik</label>
                                <div class="col-sm-6">
                                    <input type="text" maxlength="4" class="form-control" id="angkatan" name="angkatan" value="<?php echo $mahasiswa['angkatan']; ?>" placeholder="Masukkan Angkatan Mahasiswa *">
                                    <?php echo form_error('angkatan', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="ttl" class="col-sm-3 control-label">Tanggal Lahir</label>
                                <div class="col-sm-6">
                                    <input type="date" class="form-control" id="ttl" name="ttl" value="<?php echo $mahasiswa['ttl']; ?>">
                                    <?php echo form_error('ttl_mahasiswa', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin</label>
                                <div class="col-md-6 col-sm-9 col-xs-12">
                                    <select class="form-control" name="jk">
                                        <?php if ($mahasiswa['jk'] == 'laki-laki') {
                                            $lakilaki = "selected";
                                            $perempuan = "";
                                        } else {
                                            $lakilaki = "";
                                            $perempuan = "selected";
                                        }; ?>
                                        <option <?php echo $lakilaki; ?> value="laki-laki"> Laki-Laki</option>
                                        <option <?php echo $perempuan; ?> value="perempuan"> Perempuan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo $mahasiswa['email']; ?>" placeholder="Masukkan Email Mahasiswa *">
                                    <?php echo form_error('email', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="telp" class="col-sm-3 control-label">No Telepon</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="telp" name="telp" value="<?php echo $mahasiswa['telp']; ?>" placeholder="Masukkan Nomor Telepon Mahasiswa *">
                                    <?php echo form_error('telp', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $mahasiswa['alamat']; ?>" placeholder="Masukkan Alamat Mahasiswa *">
                                    <?php echo form_error('alamat', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?php echo site_url('manajemen_user/mahasiswa'); ?>" button type="submit" class="btn btn-warning"><i class="fa fa-rotate-left"></i> Kembali</a>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Edit</button>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>