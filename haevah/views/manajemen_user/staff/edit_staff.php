<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Manajemen Karyawan/ Staff
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Karyawan/ Staff</h3>
                    </div>

                    <!-- Form -->
                    <br />
                    <form method="post" action="" class="form-horizontal">
                        <input type="hidden" name="id" value="<?php echo $staff['nip']; ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="nip" class="col-sm-3 control-label">NIP</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="nip" name="nip" value="<?php echo $staff['nip']; ?>" disabled>
                                    <?php echo form_error('nip', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="nama_staff" class="col-sm-3 control-label">Nama </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="nama_staff" name="nama_staff" value="<?php echo $staff['nama_staff']; ?>" placeholder="Masukkan Nama Karyawan/ Staff *">
                                    <?php echo form_error('nama_staff', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Bagian</label>
                                <div class="col-md-6 col-sm-9 col-xs-12">
                                    <select class="form-control" name="bagian">
                                        <?php if ($staff['bagian'] == 'Pustakawan') {
                                            $pustakawan = "selected";
                                            $laboran = "";
                                            $teknisi = "";
                                            $tenaga_administrasi = "";
                                        } elseif ($staff['bagian'] == 'Laboran') {
                                            $pustakawan = "";
                                            $laboran = "selected";
                                            $teknisi = "";
                                            $tenaga_administrasi = "";
                                        } elseif ($staff['bagian'] == 'Teknisi') {
                                            $pustakawan = "";
                                            $laboran = "";
                                            $teknisi = "selected";
                                            $tenaga_administrasi = "";
                                        } else {
                                            $pustakawan = "";
                                            $laboran = "";
                                            $teknisi = "";
                                            $tenaga_administrasi = "selected";
                                        }; ?>
                                        <option <?php echo $pustakawan; ?> value="Pustakawan"> Pustakawan</option>
                                        <option <?php echo $laboran; ?> value="Laboran"> Laboran</option>
                                        <option <?php echo $teknisi; ?> value="Teknisi"> Teknisi</option>
                                        <option <?php echo $tenaga_administrasi; ?> value="Tenaga Administrasi"> Tenaga Administrasi</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="ttl" class="col-sm-3 control-label">Tanggal Lahir</label>
                                <div class="col-sm-6">
                                    <input type="date" class="form-control" id="ttl" name="ttl" value="<?php echo $staff['ttl']; ?>">
                                    <?php echo form_error('ttl', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin</label>
                                <div class="col-md-6 col-sm-9 col-xs-12">
                                    <select class="form-control" name="jk">
                                        <?php if ($staff['jk'] == 'laki-laki') {
                                            $lakilaki = "selected";
                                            $perempuan = "";
                                        } else {
                                            $lakilaki = "";
                                            $perempuan = "selected";
                                        }; ?>
                                        <option <?php echo $lakilaki; ?> value="laki-laki"> Laki-Laki</option>
                                        <option <?php echo $perempuan; ?> value="perempuan"> Perempuan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo $staff['email']; ?>" placeholder="Masukkan Email Karyawan/ Staff *">
                                    <?php echo form_error('email', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="telp" class="col-sm-3 control-label">No Telepon</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="telp" name="telp" value="<?php echo $staff['telp']; ?>" placeholder="Masukkan Nomor Telepon Karyawan/ Staff *">
                                    <?php echo form_error('telp', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $staff['alamat']; ?>" placeholder="Masukkan Alamat Karyawan/ Staff *">
                                    <?php echo form_error('alamat', '<small class="text-danger pl-3">', '</small>') ?>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?php echo site_url('manajemen_user/staff'); ?>" button type="submit" class="btn btn-warning"><i class="fa fa-rotate-left"></i> Kembali</a>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Edit</button>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>