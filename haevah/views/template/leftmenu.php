<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                        <?php
                        $idlevel = $this->session->userdata('id_level');
                        if($idlevel==1){
                            $path_image = "uploads/admin/";
                        }elseif($idlevel==2){
                            $path_image = "uploads/dosen/";
                        }elseif($idlevel==3){
                            $path_image = "uploads/staff/";
                        }else{
                            $path_image = "uploads/mahasiswa/";
                        }
                        ?>

                <img src="<?= base_url($path_image).''.$this->session->userdata('foto'); ?> " class="img-circle" alt="User Image">

            </div>
            <div class="pull-left info">
                <p><?= $this->session->userdata('nama'); ?>
                </p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>

            </div>
        </div>
        <br>

        <!-- Bagian Admin -->

        <?php
        if ($this->session->userdata('id_level') == '1') {
            ?>
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li>
                    <a href="<?php echo site_url('dashboard'); ?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>

                <li class="treeview">
                    <a>
                        <i class="fa fa-users"></i> <span> Manajemen User</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('manajemen_user'); ?>"><i class="fa fa-circle-o"></i> Dosen</a></li>
                        <li><a href="<?php echo site_url('manajemen_user/staff'); ?>"><i class="fa fa-circle-o"></i> Karyawan/Staff</a></li>
                        <li><a href="<?php echo site_url('manajemen_user/mahasiswa'); ?>"><i class="fa fa-circle-o"></i> Mahasiswa</a></li>
                    </ul>
                </li>

                <li>
                    <a href="<?php echo site_url('manajemen_periode'); ?>">
                        <i class="fa fa-bell"></i> <span>Manajemen Periode </span>
                    </a>
                </li>

                <li class="treeview">
                    <a>
                        <i class="fa fa-edit"></i> <span>Manajemen Kuesioner</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('manajemen_kuesioner/kelola'); ?>"><i class="fa fa-circle-o"></i>Kuesioner Tata Kelola</a></li>
                        <li><a href="<?php echo site_url('manajemen_kuesioner/akademik'); ?>"><i class="fa fa-circle-o"></i>Kuesioner Akademik</a></li>

                    </ul>
                </li>
            </ul>

            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">PENILAIAN</li>
                <li class="treeview">
                    <a>
                        <i class="fa fa-institution"></i> <span> Kinerja Tata Kelola</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url(''); ?>"><i class="fa fa-circle-o"></i>Importance</a></li>
                        <li><a href="<?php echo site_url(''); ?>"><i class="fa fa-circle-o"></i>Performance</a></li>
                        <li><a href="<?php echo site_url(''); ?>"><i class="fa fa-circle-o"></i>Result</a></li>
                        <li><a href="<?php echo site_url(''); ?>"><i class="fa fa-circle-o"></i>CSI</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a>
                        <i class="fa fa-book"></i> <span> Kinerja Akademik</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url(''); ?>"><i class="fa fa-circle-o"></i>Importance</a></li>
                        <li><a href="<?php echo site_url(''); ?>"><i class="fa fa-circle-o"></i>Performance</a></li>
                        <li><a href="<?php echo site_url(''); ?>"><i class="fa fa-circle-o"></i>Result</a></li>
                        <li><a href="<?php echo site_url(''); ?>"><i class="fa fa-circle-o"></i>CSI</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="fa fa-copy"></i> <span> Laporan Kinerja Layanan </span>
                    </a>
                </li>
            </ul>

            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">UTILITIES</li>
                <li>
                    <a href="<?php echo site_url('profile'); ?>">
                        <i class="fa fa-user"></i> <span>Profile</span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('login/logout'); ?>" class="tombol-logout"></i>
                        <i class="fa fa-sign-out"></i> <span> Logout</span>
                    </a>
                </li>
            </ul>

            <!-- Bagian Dosen -->

        <?php
    } elseif ($this->session->userdata('id_level') == '2') {
        ?>

            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li>
                    <a href="<?php echo site_url('dashboard/dosen'); ?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo site_url('pengisian/dosen'); ?>">
                        <i class="fa fa-edit"></i> <span> Pengisian Kuesioner</span>

                    </a>
                </li>
            </ul>

            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">UTILITIES</li>
                <li>
                    <a href="<?php echo site_url('profile/dosen'); ?>">
                        <i class="fa fa-user"></i> <span>Profile</span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('login/logout'); ?>" class="tombol-logout"></i>
                        <i class="fa fa-sign-out"></i> <span> Logout</span>
                    </a>
                </li>
            </ul>

            <!-- Bagian Karyawan/ Staff -->

        <?php
    } elseif ($this->session->userdata('id_level') == '3') {
        ?>

            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li>
                    <a href="<?php echo site_url('dashboard/staff'); ?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo site_url('pengisian/staff'); ?>">
                        <i class="fa fa-edit"></i> <span> Pengisian Kuesioner</span>

                    </a>
                </li>
            </ul>

            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">UTILITIES</li>
                <li>
                    <a href="<?php echo site_url('profile/staff'); ?>">
                        <i class="fa fa-user"></i> <span>Profile</span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('login/logout'); ?>" class="tombol-logout"></i>
                        <i class="fa fa-sign-out"></i> <span> Logout</span>
                    </a>
                </li>
            </ul>


            <!-- Bagian Mahasiswa -->

        <?php
    } elseif ($this->session->userdata('id_level') == '4') {
        ?>

            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li>
                    <a href="<?php echo site_url('dashboard/mahasiswa'); ?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo site_url('pengisian/mahasiswa'); ?>">
                        <i class="fa fa-edit"></i> <span> Pengisian Kuesioner</span>

                    </a>
                </li>
            </ul>

            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">UTILITIES</li>
                <li>
                    <a href="<?php echo site_url('profile/mahasiswa'); ?>">
                        <i class="fa fa-user"></i> <span>Profile</span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('login/logout'); ?>" class="tombol-logout"></i>
                        <i class="fa fa-sign-out"></i> <span> Logout</span>
                    </a>
                </li>
            </ul>


        <?php
    } else {
        echo '';
    }
    ?>

    </section>
    <!-- /.sidebar -->
</aside>