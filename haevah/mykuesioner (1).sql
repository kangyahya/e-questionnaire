-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 06 Bulan Mei 2020 pada 20.01
-- Versi server: 10.3.20-MariaDB-1
-- Versi PHP: 7.3.15-3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mykuesioner`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username_admin` varchar(50) NOT NULL,
  `password_admin` varchar(256) NOT NULL,
  `nama_admin` varchar(125) NOT NULL,
  `jk` varchar(20) NOT NULL,
  `ttl` date NOT NULL,
  `foto_admin` varchar(30) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `alamat` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `username_admin`, `password_admin`, `nama_admin`, `jk`, `ttl`, `foto_admin`, `telp`, `alamat`, `email`, `id_level`) VALUES
(1, 'haevah_amri', '225e39351161453005cf209892ce590a', 'Haevah Reza', 'perempuan', '1998-06-04', 'Haevah_Reza.jpg', '089685359996', 'Cirebon', 'haevahrezaamri@gmail.com', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aspek`
--

CREATE TABLE `aspek` (
  `id_aspek` int(11) NOT NULL,
  `nama_aspek` varchar(50) NOT NULL,
  `typeaspek` enum('akademik','kelola') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aspek`
--

INSERT INTO `aspek` (`id_aspek`, `nama_aspek`, `typeaspek`) VALUES
(1, 'Reliability', 'akademik'),
(2, 'Responsiveness', 'akademik'),
(3, 'Assurance', 'akademik'),
(4, 'Emphaty', 'akademik'),
(5, 'Tangible', 'akademik'),
(6, 'Organisasi dan Pekerjaan ', 'kelola'),
(7, 'Pengembangan karir atau kompetensi', 'kelola'),
(8, 'Jaminan atau kepastian (Assurance)', 'kelola'),
(9, 'Sarana dan prasarana', 'kelola');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `nid` int(11) NOT NULL,
  `nama_dosen` varchar(50) NOT NULL,
  `ttl` date NOT NULL,
  `jk` varchar(10) NOT NULL,
  `alamat` varchar(125) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`nid`, `nama_dosen`, `ttl`, `jk`, `alamat`, `telp`, `email`, `foto`, `id_level`) VALUES
(2016102001, 'Haevah', '2020-05-02', 'perempuan', 'Cirebon', '019724890', 'amri@gmail.com', '2016102001.jpg', 2),
(2016102002, 'Haveah', '2020-05-06', 'Laki-laki', 'Jl. Kp. Kesunean Gg. Bhakti RT. 004/008 Kesepuhan                                                ', '089656729025', 'inikangyahya@gmail.com', 'default.png', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuesioner`
--

CREATE TABLE `kuesioner` (
  `id_kuesioner` int(11) NOT NULL,
  `pertanyaan` varchar(525) NOT NULL,
  `id_aspek` int(11) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kuesioner`
--

INSERT INTO `kuesioner` (`id_kuesioner`, `pertanyaan`, `id_aspek`, `id_level`) VALUES
(15, 'Dosen memulai perkuliahan sesuai waktu yang telah ditentukan', 1, 4),
(16, 'Dosen mengakhiri perkuliahan sesuai waktu yang telah ditentukan', 1, 4),
(17, 'Dosen membuat SAP/RPS, kontrak kuliah, dan aturan perkuliahan yang disampaikan dengan jelas pada pertemuan pertama', 1, 4),
(18, 'Dosen membuat dan menyampaikan rencana evaluasi perkuliahan pada pertemuan pertama', 1, 4),
(19, 'Dosen menyusun dan memberikan bahan ajar untuk materi yang diberikan', 1, 4),
(20, 'Dosen selalu membagikan hasil evaluasi dengan nilai yang obyektif', 1, 4),
(21, 'Perkuliahan dilaksanakan sesuai dengan jumlah pertemuan yang telah ditentukan (minimal 12 kali pertemuan dan maksimal 14 kali pertemuan)', 1, 4),
(22, 'Dosen Pembimbing Akademik (DPA) memberikan pembimbingan konseling yang jelas dan dimengerti ', 1, 4),
(23, 'Dosen Pembimbing Proyek/Skripsi/Tugas Akhir memberikan pembimbingan yang jelas dan dimengerti', 1, 4),
(24, 'Staf BAAK dan BAUK memiliki kemampuan, pengetahuan, dan kecakapan yang tinggi dalam menjalankan tugasnya ', 1, 4),
(25, 'Prosedur penyampaian informasi BAAK dan BAUK jelas dan mudah dimengerti', 1, 4),
(26, 'Staf BAAK dan BAUK selalu ada sesuai jadwal', 1, 4),
(27, 'Informasi yang diberikan BAAK dan BAUK dapat diandalkan', 1, 4),
(28, 'Staf Perpustakaan selalu ada sesuai jadwal ', 1, 4),
(29, 'Informasi yang diberikan Perpustakaan dapat diandalkan ', 1, 4),
(30, 'Tenaga teknisi memiliki kemampuan dalam menangani kesalahan perangkat penunjang pembelajaran ', 1, 4),
(31, 'Dosen mudah ditemui untuk keperluan konsultasi materi kuliah atau laporan', 2, 4),
(32, 'Ketanggapan Dosen dalam menjawab pertanyaan dari Mahasiswa', 2, 4),
(33, 'Ketanggapan Dosen Pembimbing Akademik (DPA) dalam bimbingan konseling/perwalian', 2, 4),
(34, 'Ketanggapan Dosen Pembimbing Proyek/Skripsi/Tugas Akhir dalam bimbingan', 2, 4),
(35, 'Prosedur pelayanan di BAAK dan BAUK tidak berbelit-belit', 2, 4),
(36, 'Proses pelayanan di BAAK dan BAUK cepat dan tepat', 2, 4),
(37, 'Staf BAAK dan BAUK memberi tanggapan yang cepat dan baik terhadap keluhan anda', 2, 4),
(38, 'Prosedur pelayanan di Perpustakaan tidak berbelit-belit', 2, 4),
(39, 'Proses pelayanan di Perpustakaan cepat dan tepat', 2, 4),
(40, 'Kesigapan tenaga penunjang (teknisi/office boy) dalam mengatur ruang kelas sebelum perkuliahan dimulai serta penanganan jika terjadi kerusakan/kesalahan/ketidaksesuaian', 2, 4),
(41, 'Kesesuaian kurikulum sesuai kebutuhan dunia kerja', 3, 4),
(42, 'Keseimbangan materi pembelajaran antara teori dan kasus di dunia nyata', 3, 4),
(43, 'Keberagaman mata kuliah pilihan dan atau konsentrasi yang ditawarkan', 3, 4),
(44, 'Kemampuan Dosen menggunakan metoda pengajaran', 3, 4),
(45, 'Kemampuan Dosen menggunakan media pembelajaran (LCD Projector, Papan Tulis, dan media penunjang lainnya) sesuai dengan kebutuhan materi perkuliahan/praktikum', 3, 4),
(46, 'Kemampuan DPA dalam memberikan solusi masalah', 3, 4),
(47, 'Kemampuan Dosen Pembimbing dalam memberikan solusi materi', 3, 4),
(48, 'Staf BAAK dan BAUK memberikan pelayanan yang memuaskan sesuai kebutuhan ', 3, 4),
(49, 'BAAK dan BAUK memberikan jaminan apabila terjadi kesalahan pada hasil kerjanya', 3, 4),
(50, 'BAAK dan BAUK memberikan kemudahan dalam akses pelayanan administrasi akademik', 3, 4),
(51, 'Staf BAAK dan BAUK memberikan perlakuan yang adil kepada setiap pengguna layanan', 3, 4),
(52, 'Dosen mudah dihubungi baik melalui telepon, email, dan medsos', 4, 4),
(53, 'Dosen bersedia membantu Mahasiswa yang mengalami kesulitan studi', 4, 4),
(54, 'Dosen bersikap baik dan bersahabat kepada Mahasiswa', 4, 4),
(55, 'Staf BAAK dan BAUK bertugas sepenuh hati memberikan pelayanan ', 4, 4),
(56, 'Komunikasi dengan staf BAAK dan BAUK berjalan baik dan lancar', 4, 4),
(57, 'Staf Perpustakaan bertugas sepenuh hati dalam memberikan pelayanan', 4, 4),
(58, 'Komunikasi dengan staf Perpustakaan berjalan dengan baik dan lancar', 4, 4),
(59, 'Ruang kuliah dan praktikum yang bersih, nyaman, dan rapi', 5, 4),
(60, 'Sarana pembelajaran yang memadai di ruang kuliah dan praktikum', 5, 4),
(61, 'Sarana bimbingan konseling dan Proyek/Skripsi/Tugas Akhir yang bersih, nyaman, dan rapi', 5, 4),
(62, 'Ruang Perpustakaan yang bersih, nyaman, dan rapi', 5, 4),
(63, 'Sarana ruang Perpustakaan yang memadai ', 5, 4),
(64, 'Koleksi buku di Perpustakaan mendukung semua mata kuliah', 5, 4),
(65, 'Ruang pelayanan dan ruang tunggu BAAK dan BAUK yang bersih, nyaman, dan rapi', 5, 4),
(66, 'Sistem Informasi yang ada di BAAK dan BAUK bekerja dengan baik dan handal ', 5, 4),
(67, 'Hotspot area nyaman dan kemudahan dalam akses jaringan Internet', 5, 4),
(68, 'Sarana ruang kegiatan kemahasiswaan yang memadai', 5, 4),
(69, 'Sarana toilet yang bersih', 5, 4),
(70, 'Sosialisasi terhadap SOTK (Strukur Organisasi dan Tata Kelola) Universitas Catur Insan Cendekia dilakukan secara efektif dan efisien', 6, 2),
(71, 'Dosen Universitas Catur Insan Cendekia bekerja sesuai SOTK dengan kompetensi yang disertai tugas pokok dan fungsinya ', 6, 2),
(72, 'Kejelasan dan kemudahan program kerja yang dijalankan oleh setiap Dosen di Universitas Catur Insan Cendekia ', 6, 2),
(73, 'Kemampuan berkomunikasi dan bekerjasama antara pimpinan dan Dosen di setiap unit kerja', 6, 2),
(74, 'Kepuasan terhadap kepemimpinan dalam pengelolaan SDM', 6, 2),
(75, 'Kepuasan terhadap sinkronisasi kebijakan pimpinan ', 6, 2),
(76, 'Kepuasan terhadap konsistensi penegakan aturan ', 6, 2),
(77, 'Kepuasan terhadap keamanan, keselamatan, dan kesehatan kerja ', 6, 2),
(78, 'Pemberian motivasi dan bimbingan untuk pencapaian prestasi kerja', 7, 2),
(79, 'Kesempatan mendapatkan tugas dan beban kerja sesuai bidang keahlian', 7, 2),
(80, 'Kesempatan mengembangan ide/gagasan dan dialog dengan pimpinan', 7, 2),
(81, 'Pemberian penghargaan atas prestasi/kerja yang baik', 7, 2),
(82, 'Pemberian hak-hak untuk kesejahteraan atas pelaksanaan tugas rutin', 7, 2),
(83, 'Kepuasan terhadap sistem remunerasi yang berlaku di Universitas Catur Insan Cendekia', 8, 2),
(84, 'Mendapat tunjangan sesuai harapan', 8, 2),
(85, 'Pemberian jaminan kesehatan, sosial, dan hari tua', 8, 0),
(86, 'Ketersediaan sarana dan prasarana pendukung kegiatan pembelajaran', 9, 2),
(87, 'Ketersediaan sarana prasarana pendukung kegiatan penelitian dan PKM', 9, 2),
(88, 'Fasilitas untuk pengembangan diri mengikuti kursus, pelatihan, seminar, Workshop, dan studi lanjut', 9, 2),
(89, 'Fasilitas memperoleh informasi dan pelayanan melakukan kegiatan penelitian', 9, 2),
(90, 'Fasilitas memperoleh informasi dan pelayanan melakukan kegiatan PKM', 9, 2),
(91, 'Ketersediaan fasilitas teknologi, informasi dan komunikasi untuk kemudahan pelayanan administrasi dan evaluasi', 9, 2),
(92, 'Ketersediaan jurnal terakreditasi sebagai media publikasi karya ilmiah', 9, 2),
(93, 'Fasilitas layanan untuk kenaikan pangkat (kemudahan untuk mendapatkan informasi dan bimbingan)', 9, 2),
(94, 'Sosialisasi terhadap SOTK (Strukur Organisasi dan Tata Kelola) Universitas Catur Insan Cendekia dilakukan secara efektif dan efisien', 6, 3),
(95, 'Kepuasan bekerja sesuai dengan tugas pokok dan fungsi di Universitas Catur Insan Cendekia ', 6, 3),
(96, 'Kejelasan dan kemudahan program kerja yang dijalankan oleh setiap Karyawan di Universitas Catur Insan Cendekia', 6, 3),
(97, 'Kemampuan berkomunikasi dan bekerjasama antara pimpinan dan Karyawan di setiap unit kerja', 6, 3),
(98, 'Kepuasan terhadap kepemimpinan dalam pengelolaan Sumber Daya Manusia', 6, 3),
(99, 'Kepuasan terhadap sinkronisasi kebijakan pimpinan di Universitas Catur Insan Cendekia', 6, 3),
(100, 'Kepuasan terhadap konsistensi penegakan aturan', 6, 3),
(101, 'Kepuasan terhadap keamanan, keselamatan, dan kesehatan kerja di Universitas Catur Insan Cendekia', 6, 3),
(102, 'Pemberian motivasi dan bimbingan untuk pencapaian prestasi kerja', 7, 3),
(103, 'Kesempatan mendapatkan tugas dan beban kerja sesuai dengan bidang keahlian', 7, 3),
(104, 'Kesempatan mengembangan ide/gagasan dan dialog dengan pimpinan', 7, 3),
(105, 'Pemberian penghargaan atas prestasi/kerja yang baik', 7, 3),
(106, 'Pemberian hak-hak untuk kesejahteraan atas pelaksanaan tugas rutin', 7, 3),
(107, 'Kepuasan terhadap sistem remunerasi yang berlaku di Universitas Catur Insan Cendekia', 8, 3),
(108, 'Mendapat tunjangan sesuai harapan ', 8, 3),
(109, 'Pemberian jaminan kesehatan, sosial, dan hari tua', 8, 3),
(110, 'Ketersediaan sarana dan prasarana pendukung dalam bekerja di Universitas Catur Insan Cendekia', 9, 3),
(111, 'Kepuasan terhadap suasana kerja di Universitas Catur Insan Cendekia', 9, 3),
(112, 'Fasilitas untuk pengembangan diri mengikuti kursus, pelatihan, seminar, dan pelatihan', 9, 3),
(113, 'Ketersediaan fasilitas teknologi, informasi dan komunikasi untuk kemudahan pelayanan administrasi dan evaluasi', 9, 3),
(114, 'Fasilitas layanan untuk kenaikan pangkat (kemudahan untuk mendapatkan informasi dan bimbingan) ', 9, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `level`) VALUES
(1, 'Administrator'),
(2, 'Dosen'),
(3, 'Karyawan/Staff'),
(4, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` int(11) NOT NULL,
  `nama_mahasiswa` varchar(50) NOT NULL,
  `ttl` date NOT NULL,
  `jk` varchar(10) NOT NULL,
  `alamat` varchar(125) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `foto` varchar(50) NOT NULL DEFAULT 'default.png',
  `prodi` varchar(40) NOT NULL,
  `angkatan` varchar(4) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama_mahasiswa`, `ttl`, `jk`, `alamat`, `telp`, `email`, `foto`, `prodi`, `angkatan`, `id_level`) VALUES
(2016102029, 'Moh. Yahya', '1995-05-20', 'perempuan', 'Perumnas Burung Jl. Nuri 4 No. 1 Larangan/Harjamukti                                                    ', '089656729025', 'inikangyahya@gmail.com', '2016102029.jpg', 'Sistem Informasi Komputerisasi Akuntansi', '2016', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `periode`
--

CREATE TABLE `periode` (
  `id_periode` int(11) NOT NULL,
  `semester` varchar(20) NOT NULL,
  `tahun_angkatan` varchar(4) NOT NULL,
  `keterangan` varchar(400) NOT NULL,
  `isaktif` enum('true','false') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `periode`
--

INSERT INTO `periode` (`id_periode`, `semester`, `tahun_angkatan`, `keterangan`, `isaktif`) VALUES
(4, 'Genap', '2017', 'Pengisian Kuesioner', 'true');

-- --------------------------------------------------------

--
-- Struktur dari tabel `staff`
--

CREATE TABLE `staff` (
  `nip` int(11) NOT NULL,
  `nama_staff` varchar(50) NOT NULL,
  `ttl` date NOT NULL,
  `jk` varchar(10) NOT NULL,
  `alamat` varchar(125) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `bagian` varchar(40) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `staff`
--

INSERT INTO `staff` (`nip`, `nama_staff`, `ttl`, `jk`, `alamat`, `telp`, `email`, `bagian`, `foto`, `id_level`) VALUES
(2016102036, 'Mas Al', '2020-05-02', 'perempuan', 'Cirebon', '0123456789', 'alfian@gmail.com', '', '2016102036.jpg', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_answer`
--

CREATE TABLE `tbl_answer` (
  `id` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `id_kuesioner` int(11) NOT NULL,
  `id_aspek` int(11) NOT NULL,
  `typeaspek` enum('akademik','kelola') NOT NULL,
  `username` varchar(20) NOT NULL,
  `jawabanHarapan` varchar(2) NOT NULL,
  `harapanK` int(11) NOT NULL,
  `harapanC` int(11) NOT NULL,
  `harapanB` int(11) NOT NULL,
  `harapanSB` int(11) NOT NULL,
  `jawabanKenyataan` varchar(2) NOT NULL,
  `kenyataanK` int(11) NOT NULL,
  `kenyataanC` int(11) NOT NULL,
  `kenyataanB` int(11) NOT NULL,
  `kenyataanSB` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_answer`
--

INSERT INTO `tbl_answer` (`id`, `id_periode`, `id_kuesioner`, `id_aspek`, `typeaspek`, `username`, `jawabanHarapan`, `harapanK`, `harapanC`, `harapanB`, `harapanSB`, `jawabanKenyataan`, `kenyataanK`, `kenyataanC`, `kenyataanB`, `kenyataanSB`) VALUES
(56, 4, 94, 6, 'kelola', '2016102036', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(57, 4, 95, 6, 'kelola', '2016102036', '4', 0, 0, 0, 1, '3', 0, 0, 1, 0),
(58, 4, 96, 6, 'kelola', '2016102036', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(59, 4, 97, 6, 'kelola', '2016102036', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(60, 4, 98, 6, 'kelola', '2016102036', '3', 0, 0, 1, 0, '2', 0, 1, 0, 0),
(61, 4, 99, 6, 'kelola', '2016102036', '2', 0, 1, 0, 0, '3', 0, 0, 1, 0),
(62, 4, 100, 6, 'kelola', '2016102036', '4', 0, 0, 0, 1, '4', 0, 0, 0, 1),
(63, 4, 101, 6, 'kelola', '2016102036', '4', 0, 0, 0, 1, '2', 0, 1, 0, 0),
(64, 4, 102, 7, 'kelola', '2016102036', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(65, 4, 103, 7, 'kelola', '2016102036', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(66, 4, 104, 7, 'kelola', '2016102036', '3', 0, 0, 1, 0, '2', 0, 1, 0, 0),
(67, 4, 105, 7, 'kelola', '2016102036', '2', 0, 1, 0, 0, '1', 1, 0, 0, 0),
(68, 4, 106, 7, 'kelola', '2016102036', '2', 0, 1, 0, 0, '3', 0, 0, 1, 0),
(69, 4, 107, 8, 'kelola', '2016102036', '1', 1, 0, 0, 0, '3', 0, 0, 1, 0),
(70, 4, 108, 8, 'kelola', '2016102036', '2', 0, 1, 0, 0, '3', 0, 0, 1, 0),
(71, 4, 109, 8, 'kelola', '2016102036', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(72, 4, 110, 9, 'kelola', '2016102036', '3', 0, 0, 1, 0, '2', 0, 1, 0, 0),
(73, 4, 111, 9, 'kelola', '2016102036', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(74, 4, 112, 9, 'kelola', '2016102036', '2', 0, 1, 0, 0, '3', 0, 0, 1, 0),
(75, 4, 113, 9, 'kelola', '2016102036', '2', 0, 1, 0, 0, '2', 0, 1, 0, 0),
(76, 4, 114, 9, 'kelola', '2016102036', '3', 0, 0, 1, 0, '2', 0, 1, 0, 0),
(77, 4, 15, 1, 'akademik', '2016102029', '2', 0, 1, 0, 0, '3', 0, 0, 1, 0),
(78, 4, 16, 1, 'akademik', '2016102029', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(79, 4, 17, 1, 'akademik', '2016102029', '4', 0, 0, 0, 1, '3', 0, 0, 1, 0),
(80, 4, 18, 1, 'akademik', '2016102029', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(81, 4, 19, 1, 'akademik', '2016102029', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(82, 4, 20, 1, 'akademik', '2016102029', '3', 0, 0, 1, 0, '2', 0, 1, 0, 0),
(83, 4, 21, 1, 'akademik', '2016102029', '2', 0, 1, 0, 0, '4', 0, 0, 0, 1),
(84, 4, 22, 1, 'akademik', '2016102029', '4', 0, 0, 0, 1, '3', 0, 0, 1, 0),
(85, 4, 23, 1, 'akademik', '2016102029', '4', 0, 0, 0, 1, '4', 0, 0, 0, 1),
(86, 4, 24, 1, 'akademik', '2016102029', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(87, 4, 25, 1, 'akademik', '2016102029', '2', 0, 1, 0, 0, '2', 0, 1, 0, 0),
(88, 4, 26, 1, 'akademik', '2016102029', '1', 1, 0, 0, 0, '2', 0, 1, 0, 0),
(89, 4, 27, 1, 'akademik', '2016102029', '2', 0, 1, 0, 0, '3', 0, 0, 1, 0),
(90, 4, 28, 1, 'akademik', '2016102029', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(91, 4, 29, 1, 'akademik', '2016102029', '2', 0, 1, 0, 0, '2', 0, 1, 0, 0),
(92, 4, 30, 1, 'akademik', '2016102029', '2', 0, 1, 0, 0, '2', 0, 1, 0, 0),
(93, 4, 31, 2, 'akademik', '2016102029', '1', 1, 0, 0, 0, '2', 0, 1, 0, 0),
(94, 4, 32, 2, 'akademik', '2016102029', '1', 1, 0, 0, 0, '2', 0, 1, 0, 0),
(95, 4, 33, 2, 'akademik', '2016102029', '1', 1, 0, 0, 0, '2', 0, 1, 0, 0),
(96, 4, 34, 2, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(97, 4, 35, 2, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(98, 4, 36, 2, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(99, 4, 37, 2, 'akademik', '2016102029', '2', 0, 1, 0, 0, '1', 1, 0, 0, 0),
(100, 4, 38, 2, 'akademik', '2016102029', '4', 0, 0, 0, 1, '1', 1, 0, 0, 0),
(101, 4, 39, 2, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(102, 4, 40, 2, 'akademik', '2016102029', '2', 0, 1, 0, 0, '1', 1, 0, 0, 0),
(103, 4, 41, 3, 'akademik', '2016102029', '4', 0, 0, 0, 1, '1', 1, 0, 0, 0),
(104, 4, 42, 3, 'akademik', '2016102029', '1', 1, 0, 0, 0, '2', 0, 1, 0, 0),
(105, 4, 43, 3, 'akademik', '2016102029', '4', 0, 0, 0, 1, '1', 1, 0, 0, 0),
(106, 4, 44, 3, 'akademik', '2016102029', '2', 0, 1, 0, 0, '2', 0, 1, 0, 0),
(107, 4, 45, 3, 'akademik', '2016102029', '4', 0, 0, 0, 1, '1', 1, 0, 0, 0),
(108, 4, 46, 3, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(109, 4, 47, 3, 'akademik', '2016102029', '2', 0, 1, 0, 0, '1', 1, 0, 0, 0),
(110, 4, 48, 3, 'akademik', '2016102029', '4', 0, 0, 0, 1, '1', 1, 0, 0, 0),
(111, 4, 49, 3, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(112, 4, 50, 3, 'akademik', '2016102029', '1', 1, 0, 0, 0, '2', 0, 1, 0, 0),
(113, 4, 51, 3, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(114, 4, 52, 4, 'akademik', '2016102029', '4', 0, 0, 0, 1, '1', 1, 0, 0, 0),
(115, 4, 53, 4, 'akademik', '2016102029', '1', 1, 0, 0, 0, '2', 0, 1, 0, 0),
(116, 4, 54, 4, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(117, 4, 55, 4, 'akademik', '2016102029', '4', 0, 0, 0, 1, '1', 1, 0, 0, 0),
(118, 4, 56, 4, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(119, 4, 57, 4, 'akademik', '2016102029', '2', 0, 1, 0, 0, '1', 1, 0, 0, 0),
(120, 4, 58, 4, 'akademik', '2016102029', '4', 0, 0, 0, 1, '2', 0, 1, 0, 0),
(121, 4, 59, 5, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(122, 4, 60, 5, 'akademik', '2016102029', '2', 0, 1, 0, 0, '1', 1, 0, 0, 0),
(123, 4, 61, 5, 'akademik', '2016102029', '4', 0, 0, 0, 1, '1', 1, 0, 0, 0),
(124, 4, 62, 5, 'akademik', '2016102029', '2', 0, 1, 0, 0, '1', 1, 0, 0, 0),
(125, 4, 63, 5, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(126, 4, 64, 5, 'akademik', '2016102029', '4', 0, 0, 0, 1, '1', 1, 0, 0, 0),
(127, 4, 65, 5, 'akademik', '2016102029', '1', 1, 0, 0, 0, '2', 0, 1, 0, 0),
(128, 4, 66, 5, 'akademik', '2016102029', '1', 1, 0, 0, 0, '4', 0, 0, 0, 1),
(129, 4, 67, 5, 'akademik', '2016102029', '1', 1, 0, 0, 0, '2', 0, 1, 0, 0),
(130, 4, 68, 5, 'akademik', '2016102029', '1', 1, 0, 0, 0, '4', 0, 0, 0, 1),
(131, 4, 69, 5, 'akademik', '2016102029', '1', 1, 0, 0, 0, '1', 1, 0, 0, 0),
(132, 4, 70, 6, 'kelola', '2016102001', '4', 0, 0, 0, 1, '3', 0, 0, 1, 0),
(133, 4, 71, 6, 'kelola', '2016102001', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(134, 4, 72, 6, 'kelola', '2016102001', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(135, 4, 73, 6, 'kelola', '2016102001', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(136, 4, 74, 6, 'kelola', '2016102001', '2', 0, 1, 0, 0, '3', 0, 0, 1, 0),
(137, 4, 75, 6, 'kelola', '2016102001', '2', 0, 1, 0, 0, '3', 0, 0, 1, 0),
(138, 4, 76, 6, 'kelola', '2016102001', '2', 0, 1, 0, 0, '4', 0, 0, 0, 1),
(139, 4, 77, 6, 'kelola', '2016102001', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(140, 4, 78, 7, 'kelola', '2016102001', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(141, 4, 79, 7, 'kelola', '2016102001', '2', 0, 1, 0, 0, '2', 0, 1, 0, 0),
(142, 4, 80, 7, 'kelola', '2016102001', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(143, 4, 81, 7, 'kelola', '2016102001', '4', 0, 0, 0, 1, '3', 0, 0, 1, 0),
(144, 4, 82, 7, 'kelola', '2016102001', '2', 0, 1, 0, 0, '2', 0, 1, 0, 0),
(145, 4, 83, 8, 'kelola', '2016102001', '2', 0, 1, 0, 0, '2', 0, 1, 0, 0),
(146, 4, 84, 8, 'kelola', '2016102001', '2', 0, 1, 0, 0, '3', 0, 0, 1, 0),
(147, 4, 86, 9, 'kelola', '2016102001', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(148, 4, 87, 9, 'kelola', '2016102001', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(149, 4, 88, 9, 'kelola', '2016102001', '3', 0, 0, 1, 0, '2', 0, 1, 0, 0),
(150, 4, 89, 9, 'kelola', '2016102001', '2', 0, 1, 0, 0, '3', 0, 0, 1, 0),
(151, 4, 90, 9, 'kelola', '2016102001', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(152, 4, 91, 9, 'kelola', '2016102001', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0),
(153, 4, 92, 9, 'kelola', '2016102001', '2', 0, 1, 0, 0, '3', 0, 0, 1, 0),
(154, 4, 93, 9, 'kelola', '2016102001', '3', 0, 0, 1, 0, '3', 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`username`, `password`, `id_level`) VALUES
('2016102001', 'fd9a51aac524e49a4701aec05efc24b77eefde88', 2),
('2016102002', '4773aa315a95da67baa55b07e94700915316f32e', 2),
('2016102029', '978c6957b21483c074452cfcc8b887c347d41ca9', 4),
('2016102036', '2fa54596481c38f69bc186a05c5cbd03736573a3', 3);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD KEY `admin_ibfk_1` (`id_level`);

--
-- Indeks untuk tabel `aspek`
--
ALTER TABLE `aspek`
  ADD PRIMARY KEY (`id_aspek`);

--
-- Indeks untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`nid`);

--
-- Indeks untuk tabel `kuesioner`
--
ALTER TABLE `kuesioner`
  ADD PRIMARY KEY (`id_kuesioner`);

--
-- Indeks untuk tabel `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indeks untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`);

--
-- Indeks untuk tabel `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id_periode`);

--
-- Indeks untuk tabel `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`nip`);

--
-- Indeks untuk tabel `tbl_answer`
--
ALTER TABLE `tbl_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `aspek`
--
ALTER TABLE `aspek`
  MODIFY `id_aspek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `dosen`
--
ALTER TABLE `dosen`
  MODIFY `nid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2016102003;

--
-- AUTO_INCREMENT untuk tabel `kuesioner`
--
ALTER TABLE `kuesioner`
  MODIFY `id_kuesioner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT untuk tabel `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `nim` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2016102030;

--
-- AUTO_INCREMENT untuk tabel `periode`
--
ALTER TABLE `periode`
  MODIFY `id_periode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `staff`
--
ALTER TABLE `staff`
  MODIFY `nip` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2016102037;

--
-- AUTO_INCREMENT untuk tabel `tbl_answer`
--
ALTER TABLE `tbl_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
