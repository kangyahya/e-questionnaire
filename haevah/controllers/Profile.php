<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends My_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }


    // Admin

    public function index()
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $id = $this->session->userdata('id');
        $data['admin'] = $this->user_model->getDataAdmin_id($id);
        $data['title'] = 'E-Quistionnaire | Profile';
        $this->pagging('profile/profile_admin', $data);
    }

    public function edit_admin()
    {
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $id = $this->session->userdata('id');
        $data['admin'] = $this->user_model->getDataAdmin_id($id);
        $data['title'] = 'E-Quistionnaire | Edit Profile';

        //form validasi set rules
        $this->form_validation->set_rules('nama', 'nama_admin', 'required|trim', [
            'required' => 'Nama tidak boleh kosong'
        ]);
        $this->form_validation->set_rules('username', 'username_admin', 'required|trim', [
            'required' => 'Username tidak boleh kosong'
        ]);
        $this->form_validation->set_rules('telp', 'telp', 'numeric|required|trim', [
            'numeric' => 'No Telepon diisi dengan format angka',
            'required' => 'No Telepon tidak boleh kosong'
        ]);

        $this->form_validation->set_rules('jk', 'Jk', 'required', [
            'required' => 'Jenis Kelamin tidak boleh kosong',
        ]);
        $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
            'required' => 'Tanggal Lahir tidak boleh kosong',
        ]);


        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Harus di isi dengan format email',
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('profile/edit_admin', $data);

            //jika form validasi benar
        } else {
            $this->user_model->editProfileAdmin();
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Success!</h4> Profil Berhasil Di Edit</div>');
            redirect('profile');
        }
    }

    public function change_passwordAdmin()
    {
        $data['title'] = 'E-Quistionnaire | Edit Profil';
        $data['session'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id')])->row_array();
        $id = $this->session->userdata('id');
        $data['admin'] = $this->user_model->getDataAdmin_id($id);

        //form validasi set rules
        $this->form_validation->set_rules('pw_baru', 'pw_baru', 'required|min_length[3]', [
            'required' => 'Password baru tidak boleh kosong',
            'min_length' => 'Harus di isi minimal 3 Karakter',
        ]);
        $this->form_validation->set_rules('pw_baru2', 'pw_baru2', 'required|min_length[3]|matches[pw_baru]', [
            'required' => 'Ulangi password tidak boleh kosong',
            'min_length' => 'Harus di isi minimal 3 Karakter',
            'matches' => 'Password tidak sama'
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('profile/edit_admin', $data);

            //jika form validasi benar
        } else {
            $password_baru = $this->input->post('pw_baru');
            $password_sekarang = md5($password_baru);

            $this->db->set('password_admin', $password_sekarang);
            $this->db->where('id_admin', $this->session->userdata('id'));
            $this->db->update('admin');

            redirect('profile');
        }
    }

    //Dosen 

    public function dosen($p=null, $s=null, $q=null)
    {
        $s = $this->uri->segment('2');
        $p = $this->uri->segment('3');
        $q = $this->uri->segment('4');
        if(empty($p)){
            $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $this->session->userdata('username');
            $data['dosen'] = $this->user_model->getDataDosen_id($id);
            $where = $this->session->userdata('username');
            $data['title'] = 'E-Quistionnaire | Profile';
            $this->pagging('profile/profile_dosen', $data);
        }elseif($p=="edit"){
            $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $this->session->userdata('username');
            $data['dosen'] = $this->user_model->getDataDosen_id($id);
            $data['title'] = 'E-Quistionnaire | Profile';

            //form validasi set rules
            $this->form_validation->set_rules('nama_dosen', 'Nama_dosen', 'required|trim', [
                'required' => 'Nama tidak boleh kosong'
            ]);

            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
                'required' => 'Email tidak boleh kosong',
                'valid_email' => 'Harus di isi dengan format email',
            ]);

            $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
                'required' => 'No Telepon tidak boleh kosong',
                'numeric' => 'Harus di isi dengan format angka'
            ]);

            $this->form_validation->set_rules('jk', 'Jk', 'required', [
                'required' => 'Jenis Kelamin tidak boleh kosong',
            ]);

            $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
                'required' => 'Tanggal Lahir tidak boleh kosong',
            ]);

            //jika form validasi salah
            if ($this->form_validation->run() == FALSE) {
                $this->pagging('profile/edit_dosen', $data);

                //jika form validasi benar
            } else {
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size']      = '2048';
                $config['file_name']       = $this->input->post('id');
                $config['upload_path']   = './uploads/dosen/';
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('foto')) {
                    if (empty($_FILES['foto']['name'])){
                        $image = $this->input->post('old_image');
                    }else{
                        $image = $this->upload->data('file_name'); 
                    }
                }else {
                 $image = $this->input->post('old_image');
                    echo $this->upload->display_errors();
                }
                        
                $id = $this->input->post('id');
                $post = $this->input->post();
                $data = [
                            'nama_dosen' =>$post['nama_dosen'],
                            'ttl'=> $post['ttl'],
                            'jk' => $post['jk'],
                            'email' => $post['email'],
                            'telp' => $post['telp'],
                            'alamat' => $post['alamat'],
                            'foto' => $image
                        ];
                $this->user_model->update('dosen', ['nid'=> $id], $data);
                $this->session->unset_userdata('foto');
                $this->session->unset_userdata('nama');
                $this->session->set_userdata(['foto'=>$data['foto'], 'nama'=>$data['nama_dosen']]);
                redirect('profile/dosen');
            }
        }elseif($p=="changepas"){
            $pw_lama = $this->input->post('pw_lama');
            $pw_baru = $this->input->post('pw_baru');
            $pw_konfir = $this->input->post('pw_baru2');
            $check = $this->user_model->checkpass('user',['username'=>$this->session->userdata('username')], ['password'=>sha1($pw_lama)]);
            if($check->num_rows() > 0){
                $this->db->where('username', $this->session->userdata('username'));
                $this->db->update('user',['password'=>sha1($pw_baru)]);
                $this->session->set_flashdata('message', '<div class="alert alert-primary background-primary" role="alert">Password Changed !</div>');
                redirect('profile/dosen');
            }else{
                echo "<script>alert('password lama salah')</script>";
                redirect('profile/dosen/edit#change_password');
            }
        }
        
    }

    public function edit_dosen()
    {
        $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $id = $this->session->userdata('username');
        $data['dosen'] = $this->user_model->getDataDosen_id($id);
        $data['title'] = 'E-Quistionnaire | Profile';

        //form validasi set rules
        $this->form_validation->set_rules('nama_dosen', 'Nama_dosen', 'required|trim', [
            'required' => 'Nama tidak boleh kosong'
        ]);

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Harus di isi dengan format email',
        ]);

        $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
            'required' => 'No Telepon tidak boleh kosong',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        $this->form_validation->set_rules('jk', 'Jk', 'required', [
            'required' => 'Jenis Kelamin tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
            'required' => 'Tanggal Lahir tidak boleh kosong',
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('profile/edit_dosen', $data);

            //jika form validasi benar
        } else {
            $this->user_model->editProfileDosen();
            redirect('profile/dosen');
        }
    }

    public function change_passwordDosen()
    {
        $data['title'] = 'E-Quistionnaire | Edit Profil';
        $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $id = $this->session->userdata('username');
        $data['dosen'] = $this->user_model->getDataDosen_id($id);

        $this->form_validation->set_rules('pw_lama', 'pw_lama', 'required|trim', [
            'required' => 'Password baru tidak boleh kosong',
        ]);
        $this->form_validation->set_rules('pw_baru', 'pw_baru', 'required|trim|min_length[3]', [
            'required' => 'Password baru tidak boleh kosong',
            'min_length' => 'Harus di isi minimal 3 Karakter',
        ]);
        $this->form_validation->set_rules('pw_baru2', 'pw_baru2', 'required|trim|min_length[3]|matches[pw_baru]', [
            'required' => 'Ulangi password tidak boleh kosong',
            'min_length' => 'Harus di isi minimal 3 Karakter',
            'matches' => 'Password tidak sama'
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('profile/edit_dosen', $data);

            //jika form validasi benar
        } else {
            $password_lama = sha1($this->input->post('pw_lama'));
            $password_baru = sha1($this->input->post('pw_baru'));

            //cek kesamaan password dan inputan
            if ($password_baru != $data['session']['password']) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger background-danger" role="alert">Wrong Password !</div>');
                redirect('profile/dosen');
            } else {
                if ($password_lama == $password_baru) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger background-danger" role="alert">New Password cannot  be the same as Old Password !</div>');
                    redirect('profile/dosen');
                } else {
                    //password sudah ok
                    $Password_sha1 = $password_baru;

                    //update nama, email dan telepon
                    $this->db->set('password', $Password_sha1);
                    $this->db->where('username', $this->session->userdata('username'));
                    $this->db->update('user');

                    $this->session->set_flashdata('message', '<div class="alert alert-primary background-primary" role="alert">Password Changed !</div>');
                    redirect('profile/dosen');
                }
            }
        }
    }



    //Karyawan atau Staff

    public function staff($p=null, $q=null)
    {
        $p = $this->uri->segment('3');
        if(empty($p)){
            $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $this->session->userdata('username');
            $data['staff'] = $this->user_model->getDataStaff_id($id);
            $where = $this->session->userdata('username');
            $data['title'] = 'E-Quistionnaire | Profile';
            $this->pagging('profile/profile_staff', $data);
        }elseif ($p == "edit") {
            $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $this->session->userdata('username');
            $data['staff'] = $this->user_model->getDataStaff_id($id);
            $data['title'] = 'E-Quistionnaire | Profile';

            //form validasi set rules
            $this->form_validation->set_rules('nama_staff', 'Nama_staff', 'required|trim', [
                'required' => 'Nama tidak boleh kosong'
            ]);


            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
                'required' => 'Email tidak boleh kosong',
                'valid_email' => 'Harus di isi dengan format email',
            ]);

            $this->form_validation->set_rules('jk', 'Jk', 'required', [
                'required' => 'Jenis Kelamin tidak boleh kosong',
            ]);

            $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
                'required' => 'Tanggal Lahir tidak boleh kosong',
            ]);

            $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
                'required' => 'No Telepon tidak boleh kosong',
                'numeric' => 'Harus di isi dengan format angka'
            ]);
            if ($this->form_validation->run() == FALSE) {
            $this->pagging('profile/edit_staff', $data);
            //jika form validasi benar
            }else{
               
                        $config['allowed_types'] = 'gif|jpg|png';
                        $config['max_size']      = '2048';
                        $config['file_name']       = $this->input->post('id');
                        $config['upload_path']   = './uploads/staff/';
                        $config['overwrite'] = TRUE;
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload('foto')) {
                            if (empty($_FILES['foto']['name'])){
                            $image = $this->input->post('old_image');
                            }else{
                                $image = $this->upload->data('file_name'); 
                            }
                        }else {
                            $image = $this->input->post('old_image');
                            echo $this->upload->display_errors();
                        }
                        
                        $id = $this->input->post('id');
                        $post = $this->input->post();
                        $data = [
                            'nama_staff' =>$post['nama_staff'],
                            'ttl'=> $post['ttl'],
                            'jk' => $post['jk'],
                            'email' => $post['email'],
                            'telp' => $post['telp'],
                            'alamat' => $post['alamat'],
                            'foto' => $image
                        ];
                        $this->user_model->update('staff', ['nip'=> $id], $data);
                        $this->session->unset_userdata('foto');
                        $this->session->unset_userdata('nama');
                        $this->session->set_userdata(['foto'=>$data['foto'], 'nama'=>$data['nama_staff']]);
                        redirect('profile/staff');
                    
                
            }
        }elseif($p=="changepass"){
                        $pw_lama = $this->input->post('pw_lama');
                        $pw_baru = $this->input->post('pw_baru');
                        $pw_konfir = $this->input->post('pw_baru2');
                        $check = $this->user_model->checkpass('user',['username'=>$this->session->userdata('username')], ['password'=>sha1($pw_lama)]);
                        if($check->num_rows() > 0){
                            $this->db->where('username', $this->session->userdata('username'));
                            $this->db->update('user',['password'=>sha1($pw_baru)]);
                            redirect('profile/staff');
                        }else{
                            echo "<script>alert('password lama salah')</script>";
                            redirect('profile/staff/edit#change_password');
                        }
        }
        
    }

    public function edit_staff()
    {
        $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $id = $this->session->userdata('username');
        $data['staff'] = $this->user_model->getDataStaff_id($id);
        $data['title'] = 'E-Quistionnaire | Profile';

        //form validasi set rules
        $this->form_validation->set_rules('nama_staff', 'Nama_staff', 'required|trim', [
            'required' => 'Nama tidak boleh kosong'
        ]);


        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Harus di isi dengan format email',
        ]);

        $this->form_validation->set_rules('jk', 'Jk', 'required', [
            'required' => 'Jenis Kelamin tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
            'required' => 'Tanggal Lahir tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
            'required' => 'No Telepon tidak boleh kosong',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('profile/edit_staff', $data);

            //jika form validasi benar
        } else {
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']      = '2048';
            $config['overwrite'] = true;
            $config['file_name']       = $this->session->userdata('nama');
            $config['upload_path']   = './assets/dist/img/profile/';
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('foto')){
			$image = $this->input->post('old_image');
            }else{
                $image = $this->upload->data('file_name'); 
            }
            $data = [
            "nama_staff" => $this->input->post('nama_staff', true),
            "bagian" => $this->input->post('bagian', true),
            "ttl" => $this->input->post('ttl', true),
            "jk" => $this->input->post('jk', true),
            "email" => $this->input->post('email', true),
            "telp" => $this->input->post('telp', true),
            "alamat" => $this->input->post('alamat', true),
            "foto" => $image,
            "id_level" => '3'
            ];
            $this->user_model->editDataStaff('staff',$data, ['nip'=>$this->input->post('id')]);
            redirect('profile/staff');
        }
    }

    public function change_passwordStaff()
    {
        $data['title'] = 'E-Quistionnaire | Edit Profil';
        $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $id = $this->session->userdata('username');
        $data['staff'] = $this->user_model->getDataStaff_id($id);

        $this->form_validation->set_rules('pw_baru', 'pw_baru', 'required|min_length[3]', [
            'required' => 'Password baru tidak boleh kosong',
            'min_length' => 'Harus di isi minimal 3 Karakter',
        ]);
        $this->form_validation->set_rules('pw_baru2', 'pw_baru2', 'required|min_length[3]|matches[pw_baru]', [
            'required' => 'Ulangi password tidak boleh kosong',
            'min_length' => 'Harus di isi minimal 3 Karakter',
            'matches' => 'Password tidak sama'
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('profile/edit_staff', $data);

            //jika form validasi benar
        } else {
            $password_baru = $this->input->post('pw_baru');
            $password_sekarang = sha1($password_baru);

            $this->db->set('password', $password_sekarang);
            $this->db->where('username', $this->session->userdata('id'));
            $this->db->update('user');

            redirect('profile/staff');
        }
    }

    //Mahasiswa

    public function mahasiswa($p=null, $q=null, $id=null)
    {
        $p = $this->uri->segment('3');
        $q = $this->uri->segment('2');
        $id = $this->uri->segment('4');
        if(empty($p)){
            $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $this->session->userdata('username');
            $data['mahasiswa'] = $this->user_model->getDataMahasiswa_id($id);
            $where = $this->session->userdata('username');
            $data['title'] = 'E-Quistionnaire | Profile';
            $this->pagging('profile/profile_mahasiswa', $data);
        }elseif($p=="edit"){
            $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
            $id = $this->session->userdata('username');
            $data['mahasiswa'] = $this->user_model->getDataMahasiswa_id($id);
            $data['title'] = 'E-Quistionnaire | Profile';

            //form validasi set rules
            $this->form_validation->set_rules('nama_mahasiswa', 'Nama_mahasiswa', 'required|trim', [
                'required' => 'Nama tidak boleh kosong'
            ]);

            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
                'required' => 'Email tidak boleh kosong',
                'valid_email' => 'Harus di isi dengan format email',
            ]);

            $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
                'required' => 'No Telepon tidak boleh kosong',
                'numeric' => 'Harus di isi dengan format angka'
            ]);

            $this->form_validation->set_rules('jk', 'Jk', 'required', [
                'required' => 'Jenis Kelamin tidak boleh kosong',
            ]);

            $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
                'required' => 'Tanggal Lahir tidak boleh kosong',
            ]);

            //jika form validasi salah
            if ($this->form_validation->run() == FALSE) {
                $this->pagging('profile/edit_mahasiswa', $data);

                //jika form validasi benar
            } else {
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size']      = '2048';
                $config['file_name']       = $this->input->post('id');
                $config['upload_path']   = './uploads/mahasiswa/';
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('foto')) {
                    if (empty($_FILES['foto']['name'])){
                        $image = $this->input->post('old_image');
                    }else{
                        $image = $this->upload->data('file_name'); 
                    }
                }else {
                 $image = $this->input->post('old_image');
                    echo $this->upload->display_errors();
                }
                        
                $id = $this->input->post('id');
                $data = [
                    "nama_mahasiswa" => $this->input->post('nama_mahasiswa', true),
                    "prodi" => $this->input->post('prodi', true),
                    "angkatan" => $this->input->post('angkatan', true),
                    "ttl" => $this->input->post('ttl', true),
                    "jk" => $this->input->post('jk', true),
                    "email" => $this->input->post('email', true),
                    "telp" => $this->input->post('telp', true),
                    "alamat" => $this->input->post('alamat', true),
                    "foto" => $image
                ];
                $this->user_model->update('mahasiswa',['nim'=>$id], $data);
                $this->session->unset_userdata('foto');
                $this->session->unset_userdata('nama');
                $this->session->set_userdata(['foto'=>$data['foto'], 'nama'=>$data['nama_mahasiswa']]);
                redirect('profile/mahasiswa');
            }
        }
    }

    public function edit_mahasiswa()
    {
        $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $id = $this->session->userdata('username');
        $data['mahasiswa'] = $this->user_model->getDataMahasiswa_id($id);
        $data['title'] = 'E-Quistionnaire | Profile';

        //form validasi set rules
        $this->form_validation->set_rules('nama_mahasiswa', 'Nama_mahasiswa', 'required|trim', [
            'required' => 'Nama tidak boleh kosong'
        ]);

        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Harus di isi dengan format email',
        ]);

        $this->form_validation->set_rules('telp', 'Telp', 'required|numeric', [
            'required' => 'No Telepon tidak boleh kosong',
            'numeric' => 'Harus di isi dengan format angka'
        ]);

        $this->form_validation->set_rules('jk', 'Jk', 'required', [
            'required' => 'Jenis Kelamin tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('ttl', 'Ttl', 'required', [
            'required' => 'Tanggal Lahir tidak boleh kosong',
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('profile/edit_mahasiswa', $data);

            //jika form validasi benar
        } else {
            $this->user_model->editProfileMahasiswa();
            redirect('profile/mahasiswa');
        }
    }

    public function change_passwordMahasiswa()
    {
        $data['title'] = 'E-Quistionnaire | Edit Profil';
        $data['session'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $id = $this->session->userdata('username');
        $data['staff'] = $this->user_model->getDataMahasiswa_id($id);

        //form validasi set rules
        $this->form_validation->set_rules('pw_lama', 'pw_lama', 'required|trim', [
            'required' => 'Password lama tidak boleh kosong'
        ]);
        $this->form_validation->set_rules('pw_baru', 'pw_baru', 'required|min_length[3]', [
            'required' => 'Password baru tidak boleh kosong',
            'min_length' => 'Harus di isi minimal 3 Karakter',
        ]);
        $this->form_validation->set_rules('pw_baru2', 'pw_baru2', 'required|min_length[3]|matches[pw_baru]', [
            'required' => 'Ulangi password tidak boleh kosong',
            'min_length' => 'Harus di isi minimal 3 Karakter',
            'matches' => 'Password tidak sama'
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('profile/edit_mahasiswa', $data);

            //jika form validasi benar
        } else {
            $password_lama = $this->input->post('pw_lama');
            $password_baru = $this->input->post('pw_baru');
            if (!sha1($password_lama, $data['user']['password'])) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Salah!</h4> Password Lama Salah</div>');
                redirect('profile/edit_mahasiswa');
            } else {
                if ($password_lama == $password_baru) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Salah!</h4> Password baru tidak boleh sama dengan yang lama</div>');
                    redirect('profile/edit_mahasiswa');
                } else {
                    //password OK
                    $password_sekarang = sha1($password_baru, PASSWORD_DEFAULT);

                    $this->db->set('password', $password_sekarang);
                    $this->db->where('username', $this->session->userdata('id'));
                    $this->db->update('user');

                    redirect('profile/mahasiswa');
                }
            }
        }
    }

}
