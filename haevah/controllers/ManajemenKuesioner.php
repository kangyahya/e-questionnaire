<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manajemen_kuesioner extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kuesioner_model');
    }

    public function index()
    {
        $data['session'] = $this->db->get_where('kuesioner', ['id_kuesioner' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'Manajemen Kuesioner';
        $data['kuesioner'] = $this->kuesioner_model->getDataKuesionerAkademik();
        $data['aspek'] = $this->db->get('aspek')->result_array();
        $this->pagging('manajemen_kuesioner/data_kuesioner_akademik', $data);
    }

    public function tambah_kuesioner_akademik()
    {
        $data['session'] = $this->db->get_where('kuesioner', ['id_kuesioner' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'E-Quistionnaire | Tambah Kuesioner';
        $data['aspek'] = $this->db->get('aspek')->result_array();

        //form validasi set rules

        $this->form_validation->set_rules('nama_aspek', 'Nama_aspek', 'required', [
            'required' => 'Aspek Penilaian tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('pertanyaan', 'Pertanyaan', 'required', [
            'required' => 'Pertanyaan tidak boleh kosong',
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == false) {
            $this->pagging('manajemen_kuesioner/tambah_kuesioner_akademik', $data);

            //jika form validasi benar
        } else {
            $this->kuesioner_model->tambahDataKuesionerAkademik();
            $this->session->set_flashdata('message', ' Di Tambahkan');
            redirect('manajemen_kuesioner/index');
        }
    }

    public function edit_periode($id)
    {
        $data['session'] = $this->db->get_where('periode', ['id_periode' => $this->session->userdata('id')])->row_array();
        $data['title'] = 'E-Quistionnaire | Edit Periode';
        $data['periode'] = $this->periode_model->getDataPeriode_id($id);

        //form validasi set rules
        $this->form_validation->set_rules('semester', 'Semester', 'required', [
            'required' => 'Semester tidak boleh kosong',
        ]);

        $this->form_validation->set_rules('tahun_angkatan', 'Tahun_angkatan', 'required', [
            'required' => 'Tahun Angkatan tidak boleh kosong',
        ]);

        //jika form validasi salah
        if ($this->form_validation->run() == FALSE) {
            $this->pagging('manajemen_periode/edit_periode', $data);

            //jika form validasi benar
        } else {
            $this->periode_model->editDataPeriode();
            $this->session->set_flashdata('message', ' Di Edit');
            redirect('manajemen_periode/index');
        }
    }

    public function hapus_kuesioner_akademik($id)
    {
        $this->kuesioner_model->hapusDataKuesionerAkademik($id);
        $this->session->set_flashdata('message', ' Di Hapus');
        redirect('manajemen_kuesioner/index');
    }
}
